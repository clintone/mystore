<?php

class m180717_102213_creating_users_table extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_users', array(
			'id'=>'pk',
			'username'=>'string NOT NULL',
			'password'=>'string NOT NULL',
			'firstname'=>'string NOT NULL',
			'lastname'=>'string NOT NULL',
			'create_time'=>'datetime NOT NULL',
			'is_active'=>"tinyint(1) NOT NULL DEFAULT '1'",
			'is_deleted'=>"tinyint(1) NOT NULL DEFAULT '0'",
		), 'ENGINE= InnoDB');

	}

	public function down()
	{
		$this->dropTable('tbl_users');
	}

	
}