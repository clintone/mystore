<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/ui/css/bootstrap.min.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/ui/css/custom.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/ui/css/font-awesome.min.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/" media="screen, projection">
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body>
	<!-- Loading Body Content From other pages -->
	<?php echo $content; ?>
	<!-- Loading Body Content From other pages -->

	<!-- Begin modals -->
	<?php $this->renderPartial('/modals/register'); ?>
	<!-- End Modals -->

	<!-- Begin Javascript -->
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/ui/js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/ui/js/bootstrap.min.js"></script>
	<!-- End Javascript -->

</body>
</html>