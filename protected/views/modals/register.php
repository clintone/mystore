<div class="modal fade" id="form_modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<form action="<?php echo Yii::app()->request->baseUrl; ?>/register" method="POST" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-body">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							<label for="firstname">Firstname</label>
							<input class="form-control" type="text" name="Users[firstname]" id="firstname" />
						</div>
						<div class="form-group">
							<label for="lastname">Lastname</label>
							<input class="form-control" type="text" name="Users[lastname]" id="lastname" />
						</div>
						<div class="form-group">
							<label for="password">Password</label>
							<input class="form-control" type="password" name="Users[password]" id="password" />
						</div>
						<hr class="hr">
						<div class="form-group">
							<label for="username">Username</label>
							<input class="form-control" type="text" name="Users[username]" id="username" />
						</div>
					</div>
				</div>
				<div style="clear:both;"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Close</button>
					<button name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> SignUp</button>
				</div>
			</div>
		</form>
	</div>
</div>