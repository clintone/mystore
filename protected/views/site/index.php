<div class="col-md-3"></div>
<div class="col-md-6 well">
	<h3 class="text-primary text-center">STORE MANAGEMENT SYSTEM</h3>
	<hr style="border-top:1px dotted #ccc;"/>
	<button class="btn btn-success" data-toggle="modal" data-target="#form_modal">
	<span class="glyphicon glyphicon-plus"></span>Create Account</button>
	<br /><br />
	<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="panel panel-primary" id="login_form">
				<div class="panel-heading">Login</div>
				<div class="panel-body">
					<form>
						<div class="form-group">
							<label>Username</label>
							<input type="text" id="username" class="form-control"/>
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" id="password" class="form-control"/>
						</div>
						<div id="status" style="display:none;"><center><label class="text-danger">Please complete the details...</label></center></div>
						<center><button type="button" id="login" class="btn btn-primary"><span class="glyphicon glyphicon-log-in"></span> Login</button></center>
					</form>
				</div>
			</div>
			<div id="detail" style="display:none;"></div>
		</div>
</div>

<?php $this->renderPartial('/modals/register'); ?>

