$(document).ready(function(){
	var error = $(
		'<center><h3>Invalid username or password</h3></center>'
	+	'<center>Going back to login..</center>'
	+	'<center><img src="ico/loading.gif" width="100px"/></center>'
	);
	
	var success = $(
		'<center><h3>Login successful</h3></center>'
	+	'<center>Logging in to homepage..</center>'
	+	'<center><img src="ico/loading.gif" width="100px"/></center>'
	);
	
	
	
	$('#login').on('click', function(){
		var username = $('#username').val();
		var password = $('#password').val();
		
			if(username != "" || password != ""){
			$.ajax({
				url: 'login.php',
				type: 'POST',
				data: {username: username, password: password},
				success: function(data){
					$('#login_form').hide();
					if(data == 'success'){
						$('#detail').attr('class', 'alert alert-success')
						success.appendTo($('#detail'));
						$('#detail').fadeIn(1000);
						setTimeout(function(){
							window.location = 'home.php';
						}, 4000);
					}else if(data == "error"){
						$('#detail').attr('class', 'alert alert-danger')
						error.appendTo($('#detail'));
						$('#detail').fadeIn(1000);
						setTimeout(function(){
							$('#detail').hide();
							$('#login_form').fadeIn(1000);
						}, 4000);
					}
					
					$('#username').val('');
					$('#password').val('');
				}
			});
	
		}else{
			$('#login').attr('disabled', 'disabled');
			$('#status').show();
			setTimeout(function(){
				$('#login').removeAttr("disabled");
			}, 2000);
			
		}
	});
});